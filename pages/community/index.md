---
title: Community
---

## Stay Up To Date

### Twitter
* [@CalyxOS](https://twitter.com/CalyxOS)
* [@calyxinsitute](https://twitter.com/calyxinstitute)

<br />

## Get Support

### Matrix, IRC and Telegram
* Matrix: [#CalyxOS:matrix.org](https://matrix.to/#/#CalyxOS:matrix.org), OR
* IRC: [#calyxos](https://webchat.freenode.net/#calyxos) on Freenode, OR
* Telegram: [CalyxOSpublic](https://t.me/CalyxOSpublic)

The three rooms above are bridged, so messages on one will go to the other and vice versa.

### Reddit
* [r/CalyxOS](https://www.reddit.com/r/CalyxOS/)

### Additional Chat Channels
* **calyxos-offtopic:** For all off-topic conversations
  * Matrix: [#calyxos-offtopic:matrix.org](https://matrix.to/#/#calyxos-offtopic:matrix.org), OR
  * IRC: [#calyxos-offtopic](https://webchat.freenode.net/#calyxos-offtopic) on Freenode, OR
  * Telegram: [CalyxOSOfftopic](https://t.me/CalyxOSOffTopic)
* **calyxos-feeds:** Live feed of code commits / gitlab activity / reddit posts
  * Matrix: [#calyxos-feeds:matrix.org](https://matrix.to/#/#calyxos-feeds:matrix.org), OR
  * IRC: [#calyxos-feeds](https://webchat.freenode.net/#calyxos-feeds) on Freenode
* **calyxos-dev:** For development related discussion
  * Matrix: [#calyxos-dev:matrix.org](https://matrix.to/#/#calyxos-dev:matrix.org), OR
  * IRC: [#calyxos-dev](https://webchat.freenode.net/#calyxos-dev) on Freenode
* **calyxos-infra:** For infrastructure related discussion
  * Matrix: [#calyxos-infra:matrix.org](https://matrix.to/#/#calyxos-infra:matrix.org), OR
  * IRC: [#calyxos-infra](https://webchat.freenode.net/#calyxos-infra) on Freenode
